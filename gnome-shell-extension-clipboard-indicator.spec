Name: gnome-shell-extension-clipboard-indicator
Version: 34
Release: 1
Summary: 'Adds a clipboard indicator to the top panel, and caches clipboard history'
License: GPL
Requires: gnome-shell
Conflicts: gnome-shell-extension-clipboard-indicator-git
#Source0: gnome-shell-extension-clipboard-indicator-34.tar.gz::https://github.com/Tudmotu/gnome-shell-extension-clipboard-indicator/archive/v34.tar.gz
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%define pkgname gnome-shell-extension-clipboard-indicator

%prep
 

%description
'Adds a clipboard indicator to the top panel, and caches clipboard history'

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    install -d "$pkgdir/usr/share/gnome-shell/extensions" && cp -a "$srcdir/$pkgname-$pkgver/." "$_/clipboard-indicator@tudmotu.com"

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

